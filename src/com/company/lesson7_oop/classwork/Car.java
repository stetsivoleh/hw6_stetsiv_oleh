package com.company.lesson7_oop.classwork;

public class Car {
    private String engine;
    private int weight;
    private int maxSpeed;

    public Car(String engine, int weight, int maxSpeed) {
        this.engine = engine;
        this.weight = weight;
        this.maxSpeed = maxSpeed;
    }

    public Car() {

    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    void start() {
        System.out.println("Start");
    }
    void stop() {
        System.out.println("Stop");
    }
}
