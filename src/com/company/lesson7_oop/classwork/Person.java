package com.company.lesson7_oop.classwork;

public class Person {

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
//        if (name == null) {
//            System.out.println("Name cant be null");
//            return;
//        }
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }





    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

