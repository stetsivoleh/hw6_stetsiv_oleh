package com.company.lesson7_oop.classwork;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Person igor = new Person();
        Person andriy = new Person();

        igor.setAge(10);
        igor.setName("Igor");


        andriy.setName("Andriy");
        andriy.setAge(15);



        System.out.println(igor);


        List<Person> people = new ArrayList<>();
        people.add(igor);
        people.add(andriy);

        for (Person person : people) {
            System.out.println(person);
        }
        System.out.println(igor.getAge());

//        String str;
//        if (str != null) {
//            System.out.println(igor.setName(str));
//        }


        System.out.println(igor.getName().equals(andriy.getName()));

        Car volvo = new Car("Volvo", 150, 2100);
        Car kopeyka = new Car();

    }
}
