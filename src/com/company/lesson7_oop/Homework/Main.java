package com.company.lesson7_oop.Homework;

public class Main {
    public static void main(String[] args) {

        Rectangle rect1 = new Rectangle();
        Rectangle rect2 = new Rectangle(12, 19);

        System.out.println("Площа прямокутника1 = " + rect1.area());
        System.out.println("Периметр прямокутника1 = " + rect1.perimeter());

        System.out.println("Площа прямокутника2 = " + rect2.area());
        System.out.println("Периметр прямокутника2 = " + rect2.perimeter());


    }
}
