package com.company.lesson7_oop.Homework;

public class Rectangle {
    public double length;
    public double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }
    public Rectangle() {
        length = 10;
        width = 15;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    double perimeter() {
        return 2 * (length + width);
    }

    double area() {
        return length * width;
    }


}
