package com.company.lesson7_oop.Homework;

public class Circle {
    private double radius;
    private double diameter;
    double pi = 3.14;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    double circleLength() {
        return 2*pi*radius;
    }
    double circleArea() {
        return pi * Math.pow(diameter/2., 2);
    }
}
