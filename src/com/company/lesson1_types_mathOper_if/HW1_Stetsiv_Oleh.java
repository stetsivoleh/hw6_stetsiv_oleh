package com.company.lesson1_types_mathOper_if;

public class HW1_Stetsiv_Oleh {
    public static void main(String[] args) {

        int a = 1;
        int b = 5;
        int c = 4;

        double discr = (b * b) - (4 * a * c);

        if (discr > 0) {
            double x1, x2;
            x1 = (-b + Math.sqrt(discr)) / (2 * a);
            x2 = (-b - Math.sqrt(discr)) / (2 * a);
            System.out.println("Корені: " + x1 + ", " + x2);
        } else if (discr == 0) {
            double x0;
            x0 = -b / (2 * a);
            System.out.println("Корінь: " + x0);
        } else {
            System.out.println("Коренів не існує");
        }

        int x = 5;
        int y;

        if (x < 10) {
            y = x + 9;
        } else if (x > 15) {
            y = x - 5;
        } else {
            y = x * x;
        }

        int a1 = 10;
        int b1 = 5;
        int c1 = 11;
        int d1 = 6;
        double res = a1 + (b1 * (c1 - d1)) / 2.;
        System.out.println("Result: " + res);
    }
}
