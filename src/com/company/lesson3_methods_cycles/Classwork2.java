package com.company.lesson3_methods_cycles;

import java.util.Scanner;

public class Classwork2 {
    public static void main(String[] args) {
//        int i = 0;
//
//        while (true) {
//            if (i == 5){
//                break;
//            }
//
//            System.out.println(i);
//            i++;
//
//            if (i < 3) {
//                continue;
//            }
//
//            System.out.println("Go");
//        }

        Scanner scanner = new Scanner(System.in);



//        while (true) {
//            System.out.println("Enter number");
//            int a = scanner.nextInt();
//
//            if (a < 10){
//                continue;
//            }
//
//            if (a > 20){
//                break;
//            }
//        }
//
//        int sum = sum(10, 5);
//        System.out.println(sum);
//
//        print("");

//        int vol = volume(5, 4, 7);
//        System.out.println(vol);

//        boolean b = is(10);
//        System.out.println(b);
//
//        boolean c = isMod(6, 5);
//        System.out.println(c);
        print(1);
        print(1.);
    }

    public static int sum(int a, int b) {
        return a + b;
    }

    public static void print(String text) {

        if (text.equals("")) {
            return;
        }
        System.out.println(text);
    }
    public static void print(int i) {
        System.out.println("int");
    }
    public static void print(double i) {
        System.out.println("double");
    }

    public static int max(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    public static int volume(int a, int b, int c) {
        return a * b * c;
    }

    public static boolean is(int a){
        return a % 5 == 0;
    }

    public static boolean isMod(int a, int b) {
        return a % b == 0;
    }


}
