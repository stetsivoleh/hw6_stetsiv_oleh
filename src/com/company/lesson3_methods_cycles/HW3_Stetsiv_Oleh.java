package com.company.lesson3_methods_cycles;

import java.util.Scanner;

public class HW3_Stetsiv_Oleh {
    public static void main(String[] args) {

        // Task 1
        Scanner scanner = new Scanner(System.in);

//        System.out.println("8 * 9 = ");
//
//        int res = scanner.nextInt();
//
//        while (res != 72) {
//            System.out.println("Try again");
//            res = scanner.nextInt();
//        }

        // Task 2
        System.out.println("Enter your password: ");

        String pass = scanner.next();

        while (!pass.equals("pass123") || !pass.equals("admin")) {
            if (pass.equals("pass123")) {
                System.out.println("Welcome!");
                break;
            } else if (pass.equals("admin")) {
                System.out.println("You're logged in as admin");
                break;
            } else {
                System.out.println("Try again");
                pass = scanner.next();
            }
        }


        // Task 3
//         int n = 90;
//
//         while (n >= 0) {
//             System.out.println(n);
//             n -= 5;
//         }
        // Task 4
//        System.out.println("Enter numbers :");
//
//        System.out.println("m = ");
//        double m = scanner.nextDouble();
//
//        System.out.println("n = ");
//        double n = scanner.nextDouble();
//
//        if (Math.abs(m - 10) > Math.abs(n - 10)) {
//            System.out.println(n);
//        } else if (Math.abs(m - 10) == Math.abs(n - 10)){
//            System.out.println(m + "," + n);
//        } else {
//            System.out.println(m);
//        }
    }
}
