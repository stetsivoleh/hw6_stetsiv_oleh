package com.company.lesson2_specificTypes_scanner_switch_string;

import java.util.Scanner;

public class HW2_Stetsiv_Oleh {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        double a = 0;
        double b = 0;

        System.out.println("Enter a: ");
        if (scanner.hasNextDouble()) {
            a = scanner.nextDouble();
        } else {
            scanner.next();
        }

        System.out.println("Enter b: ");


        if (scanner.hasNextDouble()) {
            b = scanner.nextDouble();
        } else {
            scanner.next();
        }

//        if (scanner.hasNextDouble()) {
//            b = scanner.nextDouble();
//        } else {
//            System.out.println("Ви допустили помилку. Спробуйте ще раз...");
//            b = scanner.nextDouble();
//        }

        System.out.println("Enter sign: ");
        String sign = scanner.next();



//        System.out.println("Enter b: ");
//        double value1 = scanner.nextDouble();

        final String sign1 = "+";
        final String sign2 = "-";
        final String sign3 = "*";
        final String sign4 = "/";

        switch (sign) {
            case sign1: {
                System.out.println("Result: " + (a + b));
                break;
            }
            case sign2: {
                System.out.println("Result: " + (a - b));
                break;
            }
            case sign3: {
                System.out.println("Result: " + (a * b));
                break;
            }
            case sign4: {
                if (b != 0) {
                    System.out.println("Result: " + (a / b));
                } else {
                    System.out.println("Don't divide on 0!");
                }
                break;
            }
            default: {
                System.out.println("Wrong sign chosen!");
            }
        }
    }
}
