package com.company.lesson2_specificTypes_scanner_switch_string;

import java.util.Scanner;

public class classwork {
    public static void main(String[] args) {

        // цілі числа
        byte b = 5; // 1 byte
        short c = 22222; // 2 byte
        int i = 1; // 4 byte = 32 bit => diapazon: [-2^31-1; 2^31-1]
        Long l = 565454647555555555L; // 8 byte

        // раціональні(дробові) числа
        double a = 4.5; // 8 byte
        float f = 5.5f; // 4 byte

        // символьні типи
        String s = "text"; // текст
        char ch = 'a'; // 2 byte
        char ch1 = 80; // = 'P' (80 - код символа)

        System.out.println(ch1);

        // boolean
        boolean isPaid = true;

//        if (true || false) {
//            System.out.println("True");
//        }
//        if (true && false) {
//            System.out.println("False");
//        }
//
//        if (isPaid) {
//            System.out.println("Вам увімкнуть світло");
//        }

        //Ввід у консоль
       Scanner scanner = new Scanner(System.in);

//        System.out.println("Введіть а: ");
//        int a1 = scanner.nextInt();
//        System.out.println("a = " + a1);
//        double d2 = scanner.nextDouble();

//        System.out.println("Enter number of apples: ");
//        int numberOfApples = scanner.nextInt();
//
//        String text = "Oleh had " + numberOfApples + " apples";
//        text += " and 3 oranges";
//        System.out.println(text);

//        String text2 = "abc";
//
//        if (text2.equals("abc") ) {
//            System.out.println("equals");
//        }
//
//        if (text2.equalsIgnoreCase("Abc") ) {
//            System.out.println("equals");
//        }
//
//        System.out.println(text2.length());
//
//        final String winter = "winter";
//        final String spring = "spring";
//        final String summer = "summer";
//        final String autumn = "autumn";

//        System.out.println("Enter season: ");
//        String season = scanner.next();

//        if (season.equalsIgnoreCase("winter")) {
//            System.out.println("Snow");
//        } else if (season.equalsIgnoreCase("spring")) {
//            System.out.println("Wind");
//        } else if (season.equalsIgnoreCase("summer")) {
//            System.out.println("Sun");
//        } else if (season.equalsIgnoreCase("autumn")) {
//            System.out.println("Rain");
//        } else {
//            System.out.println("Error");
//        }

//        final String red = "red";
//        final String yellow = "yellow";
//        final String green = "green";

//        System.out.println("Enter color: ");
//        String color = scanner.next();

//        if (color.equalsIgnoreCase("red")) {
//            System.out.println("Don't drive!");
//        } else if (color.equalsIgnoreCase("yellow")) {
//            System.out.println("Get ready!");
//        } else if (color.equalsIgnoreCase("green")) {
//            System.out.println("Start driving!");
//        } else {
//            System.out.println("WTF");
//        }

//        switch (season) {
//            case winter: {
//                System.out.println("Snow");
//                break;
//            }
//            case spring: {
//                System.out.println("Wind");
//                break;
//            }
//            case summer: {
//                System.out.println("Sun");
//                break;
//            }
//            case autumn: {
//                System.out.println("Rain");
//                break;
//            }
//            default: {
//                System.out.println("Error");
//            }
//        }

//        switch (color) {
//            case red: {
//                System.out.println("Don't drive!");
//                break;
//            }
//            case yellow: {
//                System.out.println("Get ready!");
//                break;
//            }
//            case green: {
//                System.out.println("Start driving!");
//                break;
//            }
//            default: {
//                System.out.println("Error");
//            }
//        }

        System.out.println("Enter a: ");

        if (scanner.hasNextDouble()) {
            a = scanner.nextDouble();
        }

        double value1 = scanner.nextDouble();
    }
}
