package com.company.lesson8_extention_polymorphism.classwork;

public class Eagle extends Animal{

    public Eagle() {
    }
    @Override
    public void move() {
        System.out.println("Eagle is flying");
    }
}
