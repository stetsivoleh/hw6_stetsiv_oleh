package com.company.lesson8_extention_polymorphism.classwork;

public class Warrior extends Unit{

    private String orden;

    public Warrior() {
    }

    public Warrior(int damage, int armor, String orden) {
        super(damage, armor);
        this.orden = orden;
    }

    public void hello() {
        System.out.println("Hello!");
    }

    @Override
    public void attack() {
        System.out.println("Warrior attacks enemy and deal " + getDamage()*2 + " damage");
    }
}
