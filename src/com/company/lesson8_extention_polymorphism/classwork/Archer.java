package com.company.lesson8_extention_polymorphism.classwork;

public class Archer extends Unit {
    @Override
    public void attack() {
        System.out.println("Archer attacks enemy and deal " + getDamage()*2 + " damage");
    }

    public void hello() {
        System.out.println("Hello");
    }
}
