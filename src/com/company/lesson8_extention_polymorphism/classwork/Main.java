package com.company.lesson8_extention_polymorphism.classwork;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Unit unit = new Unit(100, 20);
        unit.greeting();

//        Warrior warrior = new Warrior();
//        warrior.hello();
//        warrior.setArmor(200);
//        warrior.setDamage(10);
//
//        warrior.attack();
//
//        Archer archer = new Archer();
//        archer.attack();

//        List<Warrior> warriors = new ArrayList<>();
//        warriors.add(new Warrior());
//        warriors.add(new Warrior());
//        warriors.add(new Warrior());
//        warriors.add(new Warrior());
//        warriors.add(new Warrior());
//
//        for (Warrior warrior1 : warriors) {
//            warrior1.attack();
//        }
//
//        List<Archer> archers = new ArrayList<>();
//        archers.add(new Archer());
//        archers.add(new Archer());
//        archers.add(new Archer());
//        archers.add(new Archer());
//        archers.add(new Archer());
//
//        for (Archer archer1 : archers) {
//            archer1.attack();
//        }

//        List<Unit> units = new ArrayList<>();
//
//        units.add(new Archer());
//        units.add(new Warrior());
//
//        for (Unit unit1 : units) {
//            unit1.attack();
//        }
//
//        Unit legolas = new Archer();
//        Archer legol = (Archer) legolas;
//
//        Warrior war = new Warrior(30, 250, "tamplier");

        List<Animal> animals = new ArrayList<>();

        animals.add(new Lion());
        animals.add(new Eagle());

        for (Animal animal1 : animals) {
            animal1.move();
        }


    }

}
