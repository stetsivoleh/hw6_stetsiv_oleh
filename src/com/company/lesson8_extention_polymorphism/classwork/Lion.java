package com.company.lesson8_extention_polymorphism.classwork;

public class Lion extends Animal{

    public Lion(){}

    @Override
    public void move() {
        System.out.println("Lion is running");
    }
}
