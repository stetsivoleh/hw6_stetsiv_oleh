package com.company.lesson8_extention_polymorphism.homework;

public class RobotDancer extends Robot {
    public RobotDancer() {
    }

    @Override
    public void work() {
        System.out.println("Я RobotDancer -  я просто танцюю");
    }
}
