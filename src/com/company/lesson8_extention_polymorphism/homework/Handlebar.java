package com.company.lesson8_extention_polymorphism.homework;

public class Handlebar {
    private String material;

    public Handlebar(String material) {
        this.material = material;
    }

    public void beep() {
        System.out.println("Beeeeeep!!!");
    }

    @Override
    public String toString() {
        return "Handlebar{" +
                "material='" + material + '\'' +
                '}';
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }
}
