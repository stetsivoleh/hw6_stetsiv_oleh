package com.company.lesson8_extention_polymorphism.homework;

public class Car {
    private int speed;
    private String brand;
    private Handlebar handlebar;
    private Wheel wheel;
    private Cab cab;

    public Car(int speed, String brand, Handlebar handlebar, Wheel wheel, Cab cab) {
        this.speed = speed;
        this.brand = brand;
        this.handlebar = handlebar;
        this.wheel = wheel;
        this.cab = cab;
    }



    public void showBrand() {
        System.out.println("The brand is " + brand);
    }

    @Override
    public String toString() {
        return "Car{" +
                "speed=" + speed +
                ", brand='" + brand + '\'' +
                ", handlebar=" + handlebar +
                ", wheel=" + wheel +
                ", cab=" + cab +
                '}';
    }

    public void showDetails() {
        System.out.println("Car details - handlebar material: " + handlebar.getMaterial() + ", wheel radius: " + wheel.getRadius() + "m , cab weight: " + cab.getWeight() + "kg");
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Handlebar getHandlebar() {
        return handlebar;
    }

    public void setHandlebar(Handlebar handlebar) {
        this.handlebar = handlebar;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public Cab getCab() {
        return cab;
    }

    public void setCab(Cab cab) {
        this.cab = cab;
    }
}
