package com.company.lesson8_extention_polymorphism.homework;

public class RobotCooker extends Robot {
    public RobotCooker() {
    }

    @Override
    public void work() {
        System.out.println("Я RobotCooker -  я просто готую");
    }
}
