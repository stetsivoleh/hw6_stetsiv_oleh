package com.company.lesson8_extention_polymorphism.homework;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        Robot robot1 = new Robot();
//        CoffeeRobot robot2 = new CoffeeRobot();
//        RobotDancer robot3 = new RobotDancer();
//        RobotCooker robot4 = new RobotCooker();
//
//        robot1.work();
//        robot2.work();
//        robot3.work();
//        robot4.work();
//
//        List<Robot> robots = new ArrayList<>();
//        robots.add(new Robot());
//        robots.add(new CoffeeRobot());
//        robots.add(new RobotCooker());
//        robots.add(new RobotDancer());
//
//        for (Robot robot0 : robots) {
//            robot0.work();
//        }

        Car car = new Car(140, "Ford", new Handlebar("leather"), new Wheel(1), new Cab(1500));

        car.showBrand();
        car.showDetails();

//        Animal leopard = new Animal("Leopard", 60, 12);
//
//        leopard.setAge(10);
//        leopard.setSpeed(55);
//
//        System.out.println("Animal name: " + leopard.getName() + " , Speed: " + leopard.getSpeed() + " kph, Age: " + leopard.getAge() + " years");

    }
}
