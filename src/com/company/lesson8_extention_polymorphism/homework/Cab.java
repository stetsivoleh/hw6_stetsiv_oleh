package com.company.lesson8_extention_polymorphism.homework;

public class Cab {
    private int weight;

    public Cab(int weight) {
        this.weight = weight;
    }

    public double lighten() {
        return weight / 2.;
    }

    @Override
    public String toString() {
        return "Cab{" +
                "weight=" + weight +
                '}';
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
