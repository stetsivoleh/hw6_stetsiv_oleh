package com.company.lesson8_extention_polymorphism.homework;

public class CoffeeRobot extends Robot {
    public CoffeeRobot() {
    }

    @Override
    public void work() {
        System.out.println("Я CoffeeRobot -  я варю каву");
    }
}
