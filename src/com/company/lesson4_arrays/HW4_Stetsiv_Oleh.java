package com.company.lesson4_arrays;

import java.util.Arrays;

public class HW4_Stetsiv_Oleh {
    public static void main(String[] args) {

//        // Task 1
//        int size1 = 100;
//        int[] array = new int[size1];
//
//        for (int i = 0; i < size1; i++) {
//            array[i] = i + 1;
//            if (array[i] % 14 == 0) {
//                System.out.println(array[i]);
//            }
//        }

        // Task 2
//        int[] array1 = {1,2,4,2,3,1,2,1};
//        Arrays.sort(array1);
//        for (int i = 1; i < array1.length; i++) {
//            int counter = 0;
//            if (array1[i] == array1[i-1]) {
//                counter++;
//                if (counter > 0) {
//                    System.out.println(array1[i]);
//                }
//            }
//        }

        // Task 3
//        int[] array2 = new int[10];
//        array2[0] = 0;
//        array2[1] = 1;
//        for (int i = 2; i < array2.length; i++) {
//            array2[i] = array2[i-1] + array2[i-2];
//        }
//        System.out.println(Arrays.toString(array2));

        // Task 4
//        int[] array3 = {2,8,7,3,1,9,10,23};
//        int max = array3[0];
//        int min = array3[0];
//        int indexMax = 0;
//        int indexMin = 0;
//        for (int i = 0; i < array3.length; i++) {
//            if (max < array3[i]) {
//                max = array3[i];
//                indexMax = i;
//            }
//            if (min > array3[i]) {
//                min = array3[i];
//                indexMin = i;
//            }
//        }
//        System.out.println(indexMax);
//        System.out.println(indexMin);

        // Task 5
//        int[] a = {2,4,1,7,9,11,3,5};
//        int[] b = {4,1,0,5,2,15,2,6};
//        int[] c = new int[a.length];
//        for (int i = 0; i < c.length; i++) {
//            c[i] = a[i] - b[i];
//        }
//        System.out.println(Arrays.toString(c));

        // Task 6
        int[] arr = {2,1,7,4,11,25,14,8,5};

        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
